let React = window.React = require('react');
let Page = require('-components/page');
let reactRender = require('-aek/react/utils/react-render');
let { VBox } = require('-components/layout');
let request = require('-aek/request');
var { InfoMessage,ErrorMessage} = require("@ombiel/aek-lib/react/components/message");
var { Listview, Item } = require("@ombiel/aek-lib/react/components/listview");
let { BasicSegment } = require('-components/segment');
var Panel = require("@ombiel/aek-lib/react/components/panel");

const Config = require('./modules/config');

let Screen = React.createClass({

  getInitialState:function(){
    return {
      loading:true,
      error: false
    };
  },

  componentDidMount:function() {
    request.action("get-surveys").end((err,res)=>{
      // check if !err and res
      if(!err && res){
        // Go normal - Reset error.
        this.setState({loading:false, error:false, surveys:res.body});
      }else{
        // Go into error state.
        this.setState({loading:false, error:true});
      }

    });
  },

  render:function() {
    let items = [];
    let message;

    // Lets hide the message until we are finished loading.
    // Lets throw an Error message incase the service is not working.
    if(!this.state.loading && !this.state.error){
      items = this.state.surveys;
      if (items.length === 0){
        message = <InfoMessage compact>{Config.language.noSurveysText}</InfoMessage>;
      } else {
        message = <InfoMessage compact>{Config.language.infoMessageText}</InfoMessage>;
      }
    }else{
      message = <ErrorMessage compact>{Config.language.errorMessageText}</ErrorMessage>;
    }

    return (
      <Page>
        <VBox>
          <BasicSegment>
            {message}
          </BasicSegment>
          <Panel style={{"position":"static"}}>
            <BasicSegment loading={this.state.loading} style={{paddingTop:"1px"}}>
              <Listview items={items} formatted itemFactory = {(item) => {
                return (
                  <Item href={item.surveyLink}>
                    <p>{item.surveyName}</p>
                  </Item>
                );
              }}/>
            </BasicSegment>
          </Panel>
        </VBox>
      </Page>
    );
  }
});

reactRender(<Screen />);
