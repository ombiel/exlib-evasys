let React = window.React = require('react');
let Button = require('-components/button');
let Group = require('-components/group');

let DropVerticalSegment = React.createClass({
  getInitialState:function() {
    let expanded = this.props.expanded ? this.props.expanded : false;
    return {
      expanded:expanded
    };
  },

  componentWillReceiveProps:function(nextProps) {
    let expandedState = this.state.expanded;
    let existingProps = this.props;
    if(nextProps.expanded !== existingProps.expanded && expandedState !== nextProps.expanded) {
      this.setState({ expanded:nextProps.expanded });
    }
  },

  toggle:function() {
    let expanded = this.state.expanded;
    if(expanded) {
      this.setState({ expanded:false });
    } else {
      this.setState({ expanded:true });
    }
  },

  render:function() {
    let { data, variation, noBorderRadius, chevronDisabled, customButtonChildComponent, customButtonText, reversed, minimalPadding, buttonFontSize, buttonIconBox, style } = this.props;
    let expandedState = this.state.expanded;
    let expandedClass = expandedState ? ' expanded' : '';

    let padding = minimalPadding ? '0.5rem' : null;
    let iconRight = customButtonText !== null ? true : false;
    let chevronIcon = expandedState ? 'chevron up' : 'chevron down';
    let borderRadius = noBorderRadius ? ' none' : '';

    // overrides
    if(chevronDisabled) {
      chevronIcon = null;
    }

    let buttonToggle = this.props.onClick ? this.props.onClick : this.toggle;
    let buttonContents = customButtonText ? <span style={{ marginRight:'0.3rem' }}>{ customButtonText }</span> : null;
    buttonContents = buttonContents === null && customButtonChildComponent ? customButtonChildComponent : null;
    buttonFontSize = buttonFontSize ? buttonFontSize : '0.9rem';
    let buttonExpander =
      <Button icon={ chevronIcon } iconBox={ buttonIconBox } variation={ variation } fluid onClick={ buttonToggle }
        style={{ textAlign:'center', padding:padding, fontSize:buttonFontSize }} iconRight={ iconRight } key='expander-button'>
        { buttonContents }
      </Button>;

    return (
      <Group style={ style } className={ 'drop-segment' + borderRadius + expandedClass }>
        { reversed ? buttonExpander : null }
        { expandedState ? data : null }
        { !reversed ? buttonExpander : null }
      </Group>
    );
  }
});

module.exports = DropVerticalSegment;
